package io.javatutorials.example;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Utility class for Date related operation
 */
public class DateUtil {
    public static DateFormat DATE_FORMAT=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static DateFormat DateTimeFormatter= java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static Date parseDate(String str) throws Exception{
        //WRONG WAY SINCE SimpleDateFormat is not thread safe
        //return DATE_FORMAT.parse(str);
        return DateTimeFormatter.parse(str);
    }
}
