package io.javatutorials.example;

import java.text.SimpleDateFormat;
import org.apache.commons.lang3.time.FastDateFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;


public class App 
{
    public static void main( String[] args ){
        App app=new App();
        app.trySimpleDateFormat();
        app.tryFastDateFormat();
        app.tryJoda();
        app.tryJ8();
    }
    //thread not safe
    public void trySimpleDateFormat(){
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date ret=df.parse("2016-08-16 12:00:00");
            System.out.println("trySimpleDateFormat()=>ret="+ret);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    //thread safe
    public void tryFastDateFormat(){
        try {
            FastDateFormat df = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");
            Object ret=df.parse("2016-08-16 12:00:00");
            System.out.println("tryFastDateFormat()=>ret="+ret);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    //thread safe for jdk6/7
    public void tryJoda(){
        try {
            org.joda.time.format.DateTimeFormatter fmt = org.joda.time.format.DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
            org.joda.time.DateTime ret = fmt.parseDateTime("2016-08-16 12:00:00");
            System.out.println("tryJoda()=>ret="+ret);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    //thread safe with jdk8
    public void tryJ8(){
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime ret = LocalDateTime.parse("2016-08-16 12:00:00", formatter);
            System.out.println("tryJ8()=>ret="+ret);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
